import React from 'react';
import './App.css';
import Home from './Pages/Home';
import { Link } from "react-router-dom";
import { ConfigProvider, Result, Button } from 'antd';
import es_ES from 'antd/es/locale/es_ES';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import AppBar from './Components/AppBar';
import AppFooter from './Components/AppFooter';
import Costs from './Pages/Costs';
import Speakers from './Pages/Speakers';
import Program from './Pages/Program'


export default function App() {
  return (
    <Router>
      <div>

        <AppBar />
        <Switchs />



        <AppFooter />
      </div>
    </Router>


  );
}

function Switchs() {
  return <Switch>
    <Route path="/programa">
      <Program />
    </Route>
    <Route path="/costos">
      <Costs />
    </Route>
    <Route path="/speakers">
      {/* <NotFound /> */}
      <Speakers></Speakers>
    </Route>
    {/* <Route path="/speakers/:id" children={<Speakers></Speakers>}>
    </Route> */}
    <Route path="/comite">
      <NotFound />
    </Route>
    <Route path="/alojamiento">
      <NotFound />
    </Route>
    <Route path="/sponsors">
      <NotFound />
    </Route>

    <Route path="/">
      <ConfigProvider locale={es_ES}>
        <Home />
      </ConfigProvider>
    </Route>
  </Switch>
}

function NotFound() {
  return <Result
    status="404"
    title="404"
    subTitle="Pagina en desarrollo... Más o menos"
    extra={<Button type="primary" ><Link to="/"> De regreso a Home</Link></Button>}
  />
}
