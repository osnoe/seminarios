import React, { Component } from 'react'
import { Link } from "react-router-dom";
import { Layout, Menu } from 'antd';
const { Header } = Layout;

export default class AppBar extends Component {
    render() {
        return (
            <Layout className="layout">
                <Header style={{ background: "#465881" }}>
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={['2']}
                        style={{ lineHeight: '64px', background: "#465881" }}
                    >
                        <Menu.Item key="home">
                            <Link to="/">Home</Link>
                        </Menu.Item>
                        <Menu.Item key="programa">
                            <Link to="/programa">Programa</Link>
                        </Menu.Item>
                        <Menu.Item key="costos">
                            <Link to="/costos">Costos</Link>
                        </Menu.Item>
                        <Menu.Item key="speakers">
                            <Link to="/speakers">Conferencistas</Link>
                        </Menu.Item>
                        <Menu.Item key="comite">
                            <Link to="/comite">Comités</Link>
                        </Menu.Item>
                        <Menu.Item key="alojamiento">
                            <Link to="/alojamiento">Alojamiento</Link>
                        </Menu.Item>
                        <Menu.Item key="sponsors">
                            <Link to="/sponsors">Sponsors</Link>
                        </Menu.Item>
                    </Menu>


                </Header>
            </Layout>

        )
    }
}