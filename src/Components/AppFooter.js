import React, { Component } from "react";
import { Layout } from "antd";
const { Footer } = Layout


export default class AppFooter extends Component {
    render() {
        return (
            <Layout>
                <Footer style={{ textAlign: 'center' }}>Congreso ©2019 Creado por MN</Footer>
            </Layout>
        )
    }
}