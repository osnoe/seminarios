import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import * as firebase from 'firebase'

const firebaseConfig = {
    apiKey: "AIzaSyC-vFJFlQINUz7kuzLRh0rIPXrHVuofhoY",
    authDomain: "congreso-web.firebaseapp.com",
    databaseURL: "https://congreso-web.firebaseio.com",
    projectId: "congreso-web",
    storageBucket: "congreso-web.appspot.com",
    messagingSenderId: "818199585367",
    appId: "1:818199585367:web:1a5ca713d98353c3eff912"
};
firebase.initializeApp(firebaseConfig)

export default firebase;