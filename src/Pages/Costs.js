import React, { Component } from "react";
import { Card, Switch, Row, Statistic, Col, Divider, Button } from "antd";
import "../Styles/Costs.css";
import Paragraph from "antd/lib/typography/Paragraph";
import Text from "antd/lib/typography/Text";
import Hero from "./Hero";

export default class Costs extends Component {
  onChange(event) {
    console.log(event);
  }
  render() {
    const profData = [
      // {
      //   title: "Inscripción completa",
      //   content: "Habitación doble.",
      //   price: "USD $550"
      // },
      {
        title: "Inscripción completa",
        price: "PENDIENTE",
        content: "Habitación sencilla."
      },
      {
        title: "Inscripción media",
        price: "PENDIENTE",
        content: "Habitación doble."
      },
      {
        title: "Inscripción sólo evento",
        price: "PENDIENTE",
        content: "Trabajo a presentar."
      }
    ];
    const studentData = [
      {
        title: "Inscripción completa",
        price: "PENDIENTE",
        content: "Habitación doble."
      },
      {
        title: "Inscripción media",
        price: "PENDIENTE",
        content: "Habitación doble."
      },
      {
        title: "Inscripción sólo evento",
        price: "PENDIENTE",
        content: "Trabajo a presentar."
      }
    ];

    return (
      <div>
        <Hero />
        <div className="background">
          <Row gutter={16}>
            <div className="priceChart">
              <Col xl={8} lg={12} sm={24}>
                <Card className="outer-card" title={<h1>Completa</h1>}>
                  <div className="inner-card">
                    {/* <h2>Inscripción Completa</h2> */}
                    {/* <Divider /> */}
                    <Statistic title="Profesor" value={profData[0].price} />
                    <Text>{profData[0].content}</Text>
                    <Divider />
                    <Statistic title="Alumno" value={studentData[0].price} />
                    <Text>{studentData[0].content}</Text>
                  </div>
                </Card>
              </Col>
              <Col xl={8} lg={12} sm={24}>
                <Card className="outer-card" title={<h1>Media</h1>}>
                  <div className="inner-card">
                    {/* <h2>Inscripción Media</h2> */}
                    {/* <Divider /> */}
                    <Statistic title="Profesor" value={profData[1].price} />
                    <Text>{profData[1].content}</Text>
                    <Divider />
                    <Statistic title="Alumno" value={studentData[1].price} />
                    <Text>{studentData[1].content}</Text>
                  </div>
                </Card>
              </Col>
              <Col xl={8} lg={12} sm={24}>
                <Card className="outer-card" title={<h1>Sólo evento</h1>}>
                  <div className="inner-card">
                    {/* <h2>Sólo evento</h2> */}
                    {/* <Divider /> */}
                    <Statistic title="Profesor" value={profData[2].price} />
                    <Text>{profData[2].content}</Text>
                    <Divider />
                    <Statistic title="Alumno" value={studentData[2].price} />
                    <Text>{studentData[2].content}</Text>
                  </div>
                </Card>
              </Col>
            </div>
            <Col span={24}>
              <Paragraph>
                La inscripción completa incluye 5 noches de hotel + tres alimentos
                diarios estilo buffet + transporte del hotel al complejo cultural
                universitario. Media Inscripción incluye; dos noches y dos días de
                alimentos con transporte. Inscripción sólo evento incluye;
                memorias y constancia de asistencia.
            </Paragraph>
            </Col>
          </Row>
          <Row type="flex" justify="center" className="button">
            <Col span={2}>
              <Button type="primary">Regístrate</Button>
            </Col>
          </Row>
          {/* <h2 className="type">Profesor</h2>
        <Row className="upper-row">
          <List
            grid={{ gutter: 16, column: profData.length }}
            dataSource={profData}
            className="list"
            renderItem={item => (
              <List.Item>
                <Card title={item.title}>
                  <Statistic value={item.price} />
                  <h3>{item.content}</h3>
                </Card>
              </List.Item>
            )}
          />
        </Row>
        <h2 className="type">Estudiante</h2>
        <Row className="lower-row">
          <List
            grid={{ gutter: 16, column: studentData.length }}
            dataSource={studentData}
            className="list"
            renderItem={item => (
              <List.Item>
                <Card title={item.title}>
                  <Statistic value={item.price} />
                  <h3>{item.content}</h3>
                </Card>
              </List.Item>
            )}
          />
        </Row> */}
        </div>
      </div>
    );
  }
}
