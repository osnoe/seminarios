import React, { Component } from "react";
import { Row, Col, Button } from 'antd';
import { Link } from "react-router-dom";
import '../Styles/Hero.css'


export default class Hero extends Component {
    render() {
        return <Row className="hero" type="flex" justify="space-around" align="middle">
            <Col xl={10} lg={12} sm={24} className="heroContainer">
                {/* Content */}
                <h1>XVI Conferencia Latinoamericana de Análisis por Técnicas de Rayos X SARX 2020</h1>
                <h2>7 - 13 de Noviembre 2020  Puebla, Pue. México</h2>
                {/* <p style={{ padding: "12px" }}>
                    Un evento lleno de platicas, talleres, meet ups y más.
                </p> */}
                <Button type="primary" shape="round" icon="" size={"large"}>
                    <Link to="/programa">Conoce más</Link>

                </Button>


            </Col>
            <Col xl={5} lg={6} sm={12} className="heroContainer">

                <div className="heroImg">
                    <img src={process.env.PUBLIC_URL + "/images/LOGOBUAP.png"} alt="Hero"></img>
                </div>
            </Col>
            <Col xl={5} lg={6} sm={12} className="heroContainer">

                <div className="heroImg">
                    <img src={process.env.PUBLIC_URL + "/images/SARX.png"} alt="Hero"></img>
                </div>
            </Col>
        </Row>
    }
}
