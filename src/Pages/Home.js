import React, { Component } from 'react'

import 'antd/dist/antd.css';
import '../index.css';

import Hero from './Hero';
import { Link } from "react-router-dom";
import { Card, Row, Col, Icon, Avatar, Button } from 'antd';
const { Meta } = Card;


class Home extends Component {
    render() {
        return (
            <div>
                <Hero></Hero>
                <Sponsors></Sponsors>
                <div>
                    <Row type="flex" justify="center" className="paymentHero hero">
                        <Col>

                            La Conferencia Latinoamericana de Análisis por Técnicas de Rayos X (SARX) es una reunión internacional sostenido en países de Latinoamérica y reúne a los mejores investigadores de diversas partes del mundo para promover el intercambio de experiencias en desarrollo de tecnologías e investigación utilizando técnicas de Rayos X. SARX nace en la década de los 80 por iniciativa de un grupo de investigadores de las ciudades de Córdoba, La Plata y Buenos Aires. Surge como seminario local que reúne a especialistas en rayos X de Argentina. Desde 1990, SARX toma carácter internacional y ha tenido lugar en diferentes países en la Región Latinoamericana. La Falda, Argentina, 1990; Punta de Tralca, Chile 1994; Cosquín, Argentina, 1996, La Falda, Argentina, 1998; San Pedro, Brasil, 2000; Nova Friburgo, Brasil, 2002; La Falda, Argentina, 2004; Arica, Chile, 2006; Cabo Frio, Brasil, 2008; Puebla, México, 2010; Santa Marta, Colombia, 2012; Carlos Paz, Argentina, 2014; Petropolis, Brasil, 2016 y el último encuentro en Pucón, Chile 2018. En todos estos eventos se han encontrado investigadores de diferentes partes del mundo. Esta Conferencia tiene lugar cada dos años y el próximo será en México en noviembre de 2020. En esta ocasión el SARX 2020 está siendo organizando por la Benemérita Universidad Autónoma de Puebla que tendrá lugar en el Complejo Cultural Universitario de la BUAP de la ciudad de Puebla, Pue., México.
            
                            El comité organizador del SARX 2020 se complace en invitarlos a participar en el evento y a formar parte de esta reunión.
            
                        </Col>
                    </Row>
                </div>

                <div>
                    <Row type="flex" justify="center" className="paymentHero hero">
                        <Col >
                            <h2>Entradas a un precio accesible para profesores, alumnos y público en general</h2>
                            <p >
                                Elige el plan que mejor se ajuste a ti.
                        </p>
                            <Button type="primary" shape="round" icon="" size={"large"}>
                                <Link to="/costos"> Conseguir Entradas</Link>
                            </Button>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }

}

export default Home;




function Sponsors() {
    return (<Row type="flex" justify="space-around" align="middle" style={{ background: "#c9d1d3" }}>
        <Col span={3} className="sponsors">
            <Icon type="gitlab" className="sponsors-icons" />
        </Col>
        <Col span={3} className="sponsors">
            <Icon type="facebook" className="sponsors-icons" />
        </Col>
        <Col span={3} className="sponsors">
            <Icon type="apple" className="sponsors-icons" />
        </Col>
        <Col span={3} className="sponsors">
            <Icon type="amazon" className="sponsors-icons" />
        </Col>
    </Row>)
}