import React from 'react'
import { Calendar, momentLocalizer } from 'react-big-calendar'
import moment from 'moment'
import "react-big-calendar/lib/css/react-big-calendar.css";
import '../Styles/Program.css'
import Hero from './Hero';

export default class Program extends React.Component {
    render() {
        const localizer = momentLocalizer(moment)

        let myEventsList = [
            {
                title: `Registro`,
                start: new Date("November 19 2019 8:00"),
                end: new Date("November 19 2019 9:00"),
            },
            {
                title: `Inaguración`,
                start: new Date("November 20 2019 9:00"),
                end: new Date("November 20 2019 9:30"),
            },
            {
                title: `Miguel Delgado`,
                start: new Date("November 21 2019 9:30"),
                end: new Date("November 21 2019 10:30"),
            },
            {
                title: `Coffe Break`,
                start: new Date("November 22 2019 11:30"),
                end: new Date("November 22 2019 12:30"),
            },
        ]
        const MyCalendar = props => (
            <div>
                <Hero />
                <div className="background">
                    <Calendar
                        localizer={localizer}
                        events={myEventsList}
                        defaultView='week'
                        defaultDate={new Date("November 19 2019 8:00")}
                        style={{ height: 500 }}
                    />
                </div>
            </div>
        )
        return (
            <MyCalendar />
        )
    }
}