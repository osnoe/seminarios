import React, { Component } from 'react'
import {Descriptions} from 'antd'
import { Card, Row, Col, Icon, Avatar, Button } from 'antd';

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams
  } from "react-router-dom";

  const { Meta } = Card;

export default class Speakers extends Component {
    render() {
        // console.log(useParams())
        return <div>
            <Ponentes></Ponentes>
        </div>

        // return (<Descriptions title="Speaker">
        //     <Descriptions.Item label="ID">{id ? id : "Sin Id"}</Descriptions.Item>
        //     <Descriptions.Item label="Nombre">Tim Fawcett</Descriptions.Item>
        //     <Descriptions.Item label="Telefono">1810000000</Descriptions.Item>
        //     <Descriptions.Item label="Pais">Estados Unidos</Descriptions.Item>
        // </Descriptions>)
    }
}

function Ponentes(){
    return (<div className="container">
                    <Row gutter={16} type="flex" justify="space-around" align="middle">
                        <Col xl={6} lg={12} sm={24}>
                            <Card key="2"
                                // style={{ width: 300 }}
                                cover={
                                    <img
                                        alt="example"
                                        src={process.env.PUBLIC_URL + "images/desk.jpg"} 
                                    />
                                }
                                actions={[

                                    <Icon type="share-alt" key="share"/>,
                                    <Icon type="eye"  key="look" />,
                                ]}
                            >
                                <Meta
                                    avatar={<Avatar src={process.env.PUBLIC_URL + "images/girl.svg"} />}
                                    title="Lorena Cornejo 🇨🇱"
                                    description="Químico Laboratorista, 1985
                                    Universidad de Chile, Arica, Chile"
                                />
                            </Card>
                        </Col>
                        <Col xl={6} lg={12} sm={24}>
                             <Card key="1" cover={
                                    <img alt="example" src={process.env.PUBLIC_URL + "images/desk.jpg"}  />}
                                actions={[
                                    <Icon type="share-alt" key="share"/>,
                                    <Icon type="eye"  key="look" />,
                                ]}
                            >
                                <Meta
                                    avatar={<Avatar src={process.env.PUBLIC_URL + "images/girl.svg"} />}
                                    title="Silvia Cuffini 🇦🇷"
                                    description="Ph.D. University of Córdoba. Argentine. 1994"
                                />
                            </Card>
                        </Col>
                        <Col xl={6} lg={12} sm={24}>
                             <Card key="2"
                                // style={{ width: 300 }}
                                cover={
                                    <img
                                        alt="example"
                                        src={process.env.PUBLIC_URL + "images/desk.jpg"} 
                                    />
                                }
                                actions={[
                                    <Icon type="share-alt" key="share"/>,
                                    <Link to={`/speakers/2`}><Icon type="eye"  key="look" /></Link>,
                                ]}
                            >
                                <Meta
                                    avatar={<Avatar src={process.env.PUBLIC_URL + "images/man.svg"} />}
                                    title="Tim Fawcett 🇺🇸"
                                    description="Executive Director, International Centre for Diffraction Data (ICDD), a non-
                                    profit scientific organization, 2001"
                                />
                            </Card>
                        </Col>
                    </Row>
                </div>)
}